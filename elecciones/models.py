from __future__ import unicode_literals

from django.db import models

#from django.contrib.auth.models import User

# Create your models here.
class Circunscripcion(models.Model):
	nombre = models.CharField(max_length=100)
	num_esc = models.IntegerField()

	def __str__(self):
		return self.nombre


class Mesa(models.Model):
	nombre = models.CharField(max_length=100)
	circunscripcion = models.ForeignKey(Circunscripcion, on_delete=models.CASCADE)

	def __str__(self):
		return self.nombre


class Partido(models.Model):
	nombre = models.CharField(max_length=100, unique=True)

	def __str__(self):
		return self.nombre

class Resultado(models.Model):
	votos = models.IntegerField(default = 0)
	partido = models.ForeignKey(Partido, on_delete=models.CASCADE)
	mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE)

	def __str__(self):
		return self.mesa.nombre + " - " + self.partido.nombre