from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth.views import redirect_to_login
from django.views import View
from elecciones.models import Circunscripcion, Mesa, Partido, Resultado
from elecciones.forms import CircunscripcionForm, MesaForm, ResultadosForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum

# Create your views here.

class ListaCircunscripciones(View):
	template_name = 'elecciones/lista_circunscripciones.html'

	def get(self, request, *args, **kwargs):
		circunscripciones = Circunscripcion.objects.all()
		return render(request, self.template_name, { 'circunscripciones' : circunscripciones })




class NuevaCircunscripcion(View):
	template_name = 'elecciones/editar_circunscripcion.html'
	form_class = CircunscripcionForm

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('elecciones:listacircunscripciones'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(NuevaCircunscripcion, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('elecciones:nuevacircunscripcion'))





class EditarCircunscripcion(View):
	template_name = 'elecciones/editar_circunscripcion.html'
	form_class = CircunscripcionForm

	def get(self, request, *args, **kwargs):
		c_id = kwargs['id']
		c = get_object_or_404(Circunscripcion, pk = c_id)
		if not c:
			return redirect(reverse('elecciones:listacircunscripciones'))

		form = self.form_class(instance = c)
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		c_id = kwargs['id']
		c = get_object_or_404(Circunscripcion, pk = c_id)
		if not c:
			return redirect(reverse('elecciones:listacircunscripciones'))
		form = self.form_class(request.POST, instance = c)

		if form.is_valid():
			form.save()
			return redirect(reverse('elecciones:listacircunscripciones'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(EditarCircunscripcion, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('elecciones:editarcircunscripcion', kwargs = { 'id' : kwargs['id'] }))




class EliminarCircunscripcion(View):

	def get(self, request, *args, **kwargs):
		return redirect(reverse('elecciones:listacircunscripciones'))

	def post(self, request, *args, **kwargs):
		c_id = request.POST.get('id')
		c = get_object_or_404(Circunscripcion, pk = c_id)
		c.delete()
		return redirect(reverse('elecciones:listacircunscripciones'))

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(EliminarCircunscripcion, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('elecciones:listacircunscripciones'))







def lista_mesas(request, idcir):
	template_name = 'elecciones/lista_mesas.html'

	if request.method == 'GET':
		cir = get_object_or_404(Circunscripcion, pk = idcir)
		if not cir:
			return redirect_to_login(reverse('elecciones:listacircunscripciones'))
		mesas = cir.mesa_set.all()
		resultados = Resultado.objects.filter(mesa__circunscripcion = cir).values('partido').annotate(recuento = Sum('votos')).order_by('-recuento')
		
		for r in resultados:
			r['partido'] = get_object_or_404(Partido, pk = r['partido'])
			r['esc'] = 0

		if resultados.count() == 1:
			resultados[0]['esc'] = cir.num_esc

		if resultados.count() > 1:
			resultados[0]['esc'] = cir.num_esc - 1
			resultados[1]['esc'] = 1

		return render(request, template_name, { 'mesas' : mesas, 'idcir' : idcir, 'resultados' : resultados })

def detalles_mesa(request, id):
	template_name = 'elecciones/detalles_mesa.html'

	if request.method == 'GET':
		mesa = get_object_or_404(Mesa, pk = id)
		resultados = mesa.resultado_set.order_by('-votos').all()

		if not mesa:
			return redirect_to_login(reverse('elecciones:listacircunscripciones'))
		return render(request, template_name, { 'mesa' : mesa, 'resultados' : resultados })


def nueva_mesa(request, idcir):
	template_name = 'elecciones/editar_mesa.html'
	form_class = MesaForm

	if not request.user.is_staff:
		return redirect_to_login(reverse('elecciones:nuevamesa', kwargs = { 'idcir' : idcir }))

	cir = get_object_or_404(Circunscripcion, pk = idcir)

	if request.method == 'POST':
		form = form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('elecciones:listamesas', kwargs = {'idcir' : form.cleaned_data['circunscripcion'].id }))
	else:
		form = form_class(initial = { 'circunscripcion' : cir })
	return render(request, template_name, { 'form' : form })

def editar_mesa(request, id):
	template_name = 'elecciones/editar_mesa.html'
	form_class = MesaForm

	if not request.user.is_staff:
		return redirect_to_login(reverse('elecciones:editarmesa', kwargs = { 'id' : id }))

	mesa = get_object_or_404(Mesa, pk = id)
	if not mesa:
		return redirect(reverse('elecciones:listacircunscripciones'))

	if request.method == 'POST':
		form = form_class(request.POST, instance = mesa)

		if form.is_valid():
			form.save()
			return redirect(reverse('elecciones:listamesas', kwargs = {'idcir' : form.cleaned_data['circunscripcion'].id }))
	else:
		form = form_class(instance = mesa)
	return render(request, template_name, { 'form' : form })

def eliminar_mesa(request):
	idmesa = request.POST.get('id')
	mesa = get_object_or_404(Mesa, pk = idmesa)
	idcir = mesa.circunscripcion.id

	if not request.user.is_staff:
		return redirect_to_login(reverse('elecciones:listamesas', kwargs = { 'idcir' : idcir }))

	if request.method == 'POST':
		mesa.delete()
		
	return redirect(reverse('elecciones:listamesas', kwargs = { 'idcir' : idcir }))







class EditarResultados(LoginRequiredMixin, View):
	template_name = 'elecciones/editar_resultados.html'
	form_class = ResultadosForm

	def get(self, request, *args, **kwargs):
		m_id = kwargs['mid']
		m = get_object_or_404(Mesa, pk = m_id)
		form = self.form_class(initial = { 'mesa' : m })
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		m_id = kwargs['mid']
		p_id = request.POST.get('partido')
		m = get_object_or_404(Mesa, pk = m_id)
		p = get_object_or_404(Partido, pk = p_id)
		r,creado = Resultado.objects.get_or_create(partido = p, mesa = m)
		form = self.form_class(request.POST, instance = r)

		if form.is_valid():
			form.save()
			return redirect(reverse('elecciones:detallesmesa', kwargs = { 'id' : m_id }))
		return render(request, self.template_name, { 'form' : form })




def index(request):
	template_name = 'elecciones/index.html'
	
	return render(request, template_name)