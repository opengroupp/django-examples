from django.forms import ModelForm
from elecciones.models import Circunscripcion, Mesa, Resultado

class CircunscripcionForm(ModelForm):
	class Meta:
		model = Circunscripcion
		exclude = ()

class MesaForm(ModelForm):
	class Meta:
		model = Mesa
		exclude = ()

class ResultadosForm(ModelForm):
	class Meta:
		model = Resultado
		exclude = ('mesa',)
