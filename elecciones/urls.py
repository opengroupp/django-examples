from django.conf.urls import url, include
from elecciones.views import ListaCircunscripciones, NuevaCircunscripcion, EditarCircunscripcion, EliminarCircunscripcion, EditarResultados
from . import views

urlpatterns = [
	url(r'circunscripciones/$', ListaCircunscripciones.as_view(), name = 'listacircunscripciones'),
	url(r'circunscripciones/nueva/$', NuevaCircunscripcion.as_view(), name = 'nuevacircunscripcion'),
	url(r'circunscripciones/editar/(?P<id>[0-9]+)/$', EditarCircunscripcion.as_view(), name = 'editarcircunscripcion'),
	url(r'circunscripciones/eliminar/$', EliminarCircunscripcion.as_view(), name = 'eliminarcircunscripcion'),
	url(r'mesas/(?P<idcir>[0-9]+)/$', views.lista_mesas, name='listamesas'),
	url(r'mesas/nueva/(?P<idcir>[0-9]+)/$', views.nueva_mesa, name = 'nuevamesa'),
	url(r'mesas/editar/(?P<id>[0-9]+)/$', views.editar_mesa, name = 'editarmesa'),
	url(r'mesas/detalle/(?P<id>[0-9]+)/$', views.detalles_mesa, name = 'detallesmesa'),
	url(r'mesas/eliminar/$', views.eliminar_mesa, name = 'eliminarmesa'),
	url(r'resultados/(?P<mid>[0-9]+)/$', EditarResultados.as_view(), name = 'editarresultados'),
	url(r'registration/', include('django.contrib.auth.urls')),
	url('^$',views.index, name= 'index'),


]
